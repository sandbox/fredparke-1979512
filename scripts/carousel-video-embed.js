(function($) {
  Drupal.Playlist = Drupal.Playlist || {}

  Drupal.behaviors.playlist_changer = {
    attach: function(context, settings) {
      $(".playlist-video-embed a").click(function(e) {
        e.preventDefault();
        $(this).closest('ul').find('a').removeClass('active');
        $(this).addClass('active');
        $(".playlist-video-embed-formatter iframe").attr("src", $(this).attr("href"));
        $(".playlist-video-embed-title").html($(this).attr("title"));
        $(".playlist-video-embed-description").html($(this).attr("alt"));
      })

      if ($('.playlist-video-inner').length == 0) {
        return;
      }

      $("ul.iscroll-list li.iscroll-item.last img").load(function() {
        var imageWidth = $(this).get(0).width;
        $("ul.iscroll-list li.iscroll-item").css('width', imageWidth);

        Drupal.Playlist.iScroll = new iScroll(document.querySelector('.playlist-video-inner'), {
          hScrollbar: false,
          onBeforeScrollStart: null,
          vScroll: false,
          checkDOMChanges: true
        });

        // Fix for IE8 where it doesn't work out the correct maxscroll.
        Drupal.Playlist.iScroll.maxScrollX = -(Drupal.Playlist.iScroll.scrollerW - $(Drupal.Playlist.iScroll.wrapper).width());

      });

      $('.playlist-video-embed-nav a').click(function(e) {
        e.preventDefault();
        var bounce = 50;
        var iscrollItemWidth = $('li.iscroll-item').width();
        var scrollTo = Drupal.Playlist.iScroll.x;
        if ($(this).hasClass('playlist-video-next')) {
          if (scrollTo - iscrollItemWidth < Drupal.Playlist.iScroll.maxScrollX) {
            scrollTo = Drupal.Playlist.iScroll.maxScrollX - bounce;
          }
          else {
            scrollTo -= iscrollItemWidth;
          }
        }
        else if ($(this).hasClass('playlist-video-prev')) {
          if (scrollTo + iscrollItemWidth > 0) {
            scrollTo = bounce;
          }
          else {
            scrollTo += iscrollItemWidth;
          }
        }
        Drupal.Playlist.iScroll.scrollTo(scrollTo, 0, 300);
      })
    }
  }
})(jQuery);
